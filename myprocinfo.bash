#!/bin/bash
# no arguments, display a menu with option for info
# about processes
while [ "$svar" != "9" ]
do
 echo ""
 echo "  1 - Hvem er jeg og hva er navnet på dette scriptet?"
 echo "  2 - Hvor lenge er det siden neste boot?"
 echo "  3 - Hva var gjennomsnittelig load siste minutt?"
 echo "  4 - Hvor mange prosesser og tråder finnes?"
 echo "  5 - hvor mange context switch'er fant sted siste sekund?"
 echo "  6 - hvor mange interrupts fant sted siste sekund?"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar
 case $svar in
  1)
    echo "Jeg er $(whoami) og kjører $0"
    read -r svar
    ;;
  2)
    echo "Uptime $(uptime | awk '{print $3,$4,$5,$6}' | sed 's/,//g')"
    read -r svar
    ;;
  3)
    echo "Antall prosesser og tråder: $(ps aux | wc -l)"
    ;;
  4)
    ctxt1=$(cat /proc/stat | grep ctxt | cut -f 2 -d " ");
    sleep 1; #venter 1 sekund.
    ctxt2=$(cat /proc/stat | grep ctxt | cut -f 2 -d " ");
    echo $((ctxt2 - ctxt1)) # sjekker hvor mange context switcher som skjer på 1 sekund
    read -r svar
    ;;
 5)
    cpuTid1=$(cat /proc/stat | head -n 1 | sed 's/cpu //g')
    sleep 1;
    cpuTid2=$(cat /proc/stat | head -n 1 | sed 's/cpu //g')

    TotalCpuTid=$(($(echo cpuTid2 | sed 's/ /+/g' | bc)-$(echo cpuTid1 | sed 's/ /+/g' | bc)))
    Kjerne=$(($(echo cpuTid2 | awk '{print $3}')-$(echo cpuTid1 | awk '{print $3}')))
    Bruker=$(($(echo cpuTid2 | awk '{print $1}')-$(echo cpuTid1 | awk '{print $1}')))
    echo $(echo "scale=2;($Kjerne*100/$TotalCpuTid)" | bc)
    echo $(echo "scale=2;($Bruker*100/TotalCpuTid)" | bc)
    read -r svar
    ;;
    #Fant ikke feilen her.
  6)
    interrupt1=$(cat /proc/stat | grep intr | sed -e 's/intr //g' -e 's/ /+/g' | bc)
    sleep 1;
    interrupt2=$(cat /proc/stat | grep intr | sed -e 's/intr //g' -e 's/ /+/g' | bc)
    echo "Antall interrupts det siste sekundet er: " $(($interrupt2-$interrupt1))
   read -r svar
  ;;
 9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg
    read -r svar
    ;;
 esac
done

