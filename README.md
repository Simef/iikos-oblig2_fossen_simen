# Obligatorisk oppgave 2 - Bash pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 7.5.a (Prosesser og tr�der)
* 8.6.c (Page faults)
* 8.6.d (En prosess sin bruk av virtuelt og fysisk minne)
* 9.4.a (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

**Simen Fossen

## Sjekkliste

* Har navnene p� gruppemedlemmene blitt skrevet inn over?
* Har l�ringsassistenter og foreleser blitt lagt til med leserettigheter?
* Er issue-tracker aktivert?
* Er pipeline aktivert, og returnerer pipelinen "Successful"?


* Gruppemedlemmer skrevet over!
* Leserettigheter gitt!
* Issue tracker aktivert
* Pipeline akrivert med "successful" return!
