#!/bin/bash

echo "Eksempel på datoformat: $(date +%Y%m%d-%H:%M:%S)"

function skrivTilFil{
Dato = $(date +%Y%m%d-%H:%M:%S)
FilNavn = "$pid-$Dato.meminfo"
nano FilNavn
echo "******* Minne info om prosess med PID $pid ********" >> $FilNavn
echo "Total bruk av virtuelt minne (VmSize): $VmSize KB" >> $FilNavn
echo "Mengde private virtuelt minne (VmData+VmStk+VmExe): $VmData KB"  >> $FilNavn
echo "Mengde shared virtuelt minne (VmLib: $VmLib KB" >> $FilNavn
echo "Total bruk av fysisk minne (VmRSS) $VmRss KB" >> $FilNavn
echo "Mengde fysisk minne som benyttes tik page table (VmPTE): $VvPTE KB" >> $FilNavn
}

for pid in $@; do #tar nummer fra kommandolinje og setter inn i pid
if [[ -e /proc/$pid/status && -r /proc/$pid/status ]]; then
status=$(cat proc/$pid/status)
VmSize=$(echo "$status" | grep VmSize | awk '{print $2}')
VmData=$(echo "$status" | egrep 'Vm[DSE][atx]' | awk '{print $2}' | sed ':a;N$!ba;s/\n/+/g' | bc )
VmLib=$(echo "$status" | grep VmLib | awk '{print $2}')
VmRss=$(echo "$status" | grep VmRSS | awk '{print $2}')
VmPTE=$(echo "$status" | grep VmPTE | awk '{print $2}')
skrivTilFil
else
echo "prosess med pid $pid finnes ikke eller så har du ikke read rettigheter"

fi

done

