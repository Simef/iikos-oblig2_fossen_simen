#!/bin/bash

if [[ -d $1 ]]; then
full=$(df -h $1 | tail -n 1 | awk '{print $5}')
filer=$(find $1 -depth -type f | awk '{print $5}')
storste =$(find $1 -depth -type f | tr '\n' '\0' | du -ah --files0-from=- | sort -h | tail -n 1)
total=$(find $1 -depth -type f | tr '\n' '\0' | du -ah --files0-from=- | awk '{print $1}' | sed ':a;N;ba;s/\n/+/g' | bc)
gjennomsnitt=$( ($total/$filer) )
hardlink=$(find /home/hoel -depth -type -f print0 | xargs --null ls -la | awk '{print $2, $9}' | sort | tail -n 1)

echo "Partisjonen $1 befinner seg på er $full full"
echo "Detf innes $filer filer."
echo "Den største er $(echo $storste | awk '{print $2}') som er $(echo $storste | awk '{print $1}') stor."
echo "Gjennomsnittelig filstørrelse er ca $gjennomsnitt bytes"
echo "Filen $(echo $hardlink | awk '{print $2}') har flest hardlinks, den har $(echo $hardlink | awk '{print $1}') harlinker."
else
echo "$1 er ikke en directory"
fi
